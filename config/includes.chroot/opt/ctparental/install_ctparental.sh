#! /bin/bash

# install_ctparental.sh --
#
#   This file permits to install CTparental softwares for the Emmabuntus Distrib.
#
#   Created on 2010-23 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################


. "$HOME/.config/user-dirs.dirs"

clear

# Non lancement du script en mode live, car cela ne sert à rien d'installer ce logiciel
if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
then

    echo "Mode live"
    exit 1

fi

# Test si utilisateur n'appartient pas au groupe sudo
if [[ $(groups | grep sudo) == "" ]] ; then

    echo "User isn't in sudo group !!"
    exit 2

fi
# Test de la présence d'Internet
# Si non présent arrêt du script
if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then
    echo "Internet is a live"
else
    echo "Internet wasn't a live !!"
    exit 3
fi

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_distribution="Emmabuntus Debian Edition 5"
base_distribution="bookworm"
nom_titre_window="Install CTparental software"
repertoire_emmabuntus=~/.config/emmabuntus
fichier_init_config_user=${repertoire_emmabuntus}/init_config_ctparental.txt
dir_install_software=/opt/ctparental
fichier_init_config_final=$dir_install_software/.init_config_ctparental.txt
delai_fenetre=20
delai_fenetre_selection=120
delai_fenetre_progression=1200 ; # temporisation de 20 minutes
nom_navigateur_defaut="falkon"

if [[ ${LANG} == fr* ]] ; then
    URL_tutoriel_CTpatental="http://ctparental-fr.emmabuntus.org"
else
    URL_tutoriel_CTpatental="http://ctparental-en.emmabuntus.org"
fi

Init=""

Init=$1

echo "Init=$Init"

source /usr/bin/emmabuntus_found_theme.sh
highlight_color=$(FOUND_HIGHLIGHT_COLOR)

if [[ ${Init} != "" || ! ( -f $fichier_init_config_user || -f $fichier_init_config_final ) ]]
then

message_demarrage="\n\\<span color=\'${highlight_color}\'>$(eval_gettext 'Do you want to install the parental control software \042CTparental\042 in your computer ?')\</span>\n\
\n\
$(eval_gettext 'If you have minor children at home, we strongly encourage you to install this software to protect children from violent, pornographic, etc. content on Internet.')\n\
\n\
\<span color=\'"'red\'"'>$(eval_gettext 'Warning:')\</span> $(eval_gettext 'for this protection to be effective, it is important that your children use accounts without administrator rights, which will be created automatically if the option below is checked. To create these accounts, put the names of your children separated by a space in the field provided. If you want to set a password for each child account, enter the passwords separated by a space and having at least 6 characters composed of letters and numbers. Otherwise leave this field empty if you don\047t want to use passwords.')\n\
\n\
\<span color=\'"'red\'"'>$(eval_gettext 'Warning:')\</span> $(eval_gettext 'It is important that your children never use your own account which, by default, does not have these protections for minors and allows the installation of third party software that may be prohibited to minors.')\n\
\n\
$(eval_gettext 'During the \042CTparental\042 installation, you will have to enter the account name and the password of the parental control administration interface. This password must contain at least 6 characters and one lowercase, one uppercase, one number, and one special character.')\n\
\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Note:')\</span> $(eval_gettext 'after its installation, CTparental allows you to also define Internet access time slots for your children via its administration interface which is located under the Applications menu (icon on the top left), then Internet section. For more information read this tutorial.')"

msg_create_child_count=$(eval_gettext 'Automatic creation of child accounts')
comment_create_child_count=$(eval_gettext 'Allows the creation of the child accounts defined below')
msg_name_child_count=$(eval_gettext 'Child accounts names :')
comment_name_child_count=$(eval_gettext 'Specify in this field the names of your children separated by a space')
name_child_count=$(eval_gettext 'girl boy')
msg_password_child_count=$(eval_gettext 'Child accounts password :')
comment_password_child_count=$(eval_gettext 'Specify in this field the passwords of your children separated by a space. If you do not want to put passwords leave this field empty.')
msg_create_guest_count=$(eval_gettext 'Create a guest account without password that will be reset at each startup')
comment_create_guest_count=$(eval_gettext 'Account for use in a school, a media library, in a public place')
msg_launch_config_ctparental=$(eval_gettext 'Launching the configuration interface')
comment_launch_config_ctparental=$(eval_gettext 'For more information on how to configure parental controls read our online tutorial')


# Initilisation des variables du tableau
HEIGHT_WINDOW=600
WIDTH_WINDOW=800
HEIGHT_CHECKBOX=160

CHECKBOX_RESTART="true"
CREATE_CHILD_COUNT="true"
CHECKBOX_CREATE_CHILD_COUNT="true"
NAME_CHILD_COUNT=${name_child_count}
PASSWORD_CHILD_COUNT=""
CREATE_GUEST_COUNT="false"
CHECKBOX_CREATE_GUEST_COUNT="true"
LAUNCH_CONFIG_CTPARENTAL="false"
CHECKBOX_LAUNCH_CONFIG_CTPARENTAL="true"


if [ $(which ${nom_navigateur_defaut}) ] ; then
    open_tutoriel_ctpatental="falkon -e ${URL_tutoriel_CTpatental}"
else
    open_tutoriel_ctpatental="firefox-esr ${URL_tutoriel_CTpatental}"
fi


export WINDOW_DIALOG='<window title="'$(eval_gettext 'CTparental software installation')'" icon-name="gtk-dialog-question" height_request="'${HEIGHT_WINDOW}'" maximize_initially="true" resizable="false">
<vbox spacing="5" space-fill="true" space-expand="true">
<text use-markup="true" wrap="true" xalign="0" justify="3" width_request="'${WIDTH_WINDOW}'">
<input>echo "'${message_demarrage}'" | sed "s%\\\%%g"</input>
</text>

<hseparator space-expand="true" space-fill="true"></hseparator>

<vbox homogeneous="true" height_request="'${HEIGHT_CHECKBOX}'">

  <checkbox active="'${CREATE_CHILD_COUNT}'" sensitive="'${CHECKBOX_CREATE_CHILD_COUNT}'" tooltip-text="'${comment_create_child_count}'">
    <variable>CREATE_CHILD_COUNT</variable>
    <label>'${msg_create_child_count}'</label>
    <action>if true enable:NAME_CHILD_COUNT</action>
    <action>if false disable:NAME_CHILD_COUNT</action>
    <action>if true enable:TEXT_NAME_CHILD_COUNT</action>
    <action>if false disable:TEXT_NAME_CHILD_COUNT</action>
    <action>if true enable:PASSWORD_CHILD_COUNT</action>
    <action>if false disable:PASSWORD_CHILD_COUNT</action>
    <action>if true enable:TEXT_PASSWORD_CHILD_COUNT</action>
    <action>if false disable:TEXT_PASSWORD_CHILD_COUNT</action>
  </checkbox>

   <hbox>
   <text sensitive="'${CREATE_CHILD_COUNT}'">
      <label>'${msg_name_child_count}'</label>
      <variable>TEXT_NAME_CHILD_COUNT</variable>
   </text>
   <entry sensitive="'${CREATE_CHILD_COUNT}'" tooltip-text="'${comment_name_child_count}'">
      <default>'${NAME_CHILD_COUNT}'</default>
      <variable>NAME_CHILD_COUNT</variable>
   </entry>
   </hbox>

   <hbox>
   <text sensitive="'${CREATE_CHILD_COUNT}'">
      <label>'${msg_password_child_count}'</label>
      <variable>TEXT_PASSWORD_CHILD_COUNT</variable>
   </text>
   <entry sensitive="'${CREATE_CHILD_COUNT}'" tooltip-text="'${comment_password_child_count}'">
      <variable>PASSWORD_CHILD_COUNT</variable>
   </entry>
   </hbox>

  <checkbox active="'${CREATE_GUEST_COUNT}'" sensitive="'${CHECKBOX_CREATE_GUEST_COUNT}'" tooltip-text="'${comment_create_guest_count}'">
    <variable>CREATE_GUEST_COUNT</variable>
    <label>'${msg_create_guest_count}'</label>
  </checkbox>

  <checkbox active="'${LAUNCH_CONFIG_CTPARENTAL}'" sensitive="'${CHECKBOX_LAUNCH_CONFIG_CTPARENTAL}'" tooltip-text="'${comment_launch_config_ctparental}'">
    <variable>LAUNCH_CONFIG_CTPARENTAL</variable>
    <label>'${msg_launch_config_ctparental}'</label>
  </checkbox>
</vbox>

 <hseparator space-expand="true" space-fill="true"></hseparator>

 <hbox spacing="10" space-expand="false" space-fill="false">
   <checkbox active="'${CHECKBOX_RESTART}'">
   <variable>CHECKBOX_RESTART</variable>
   <label>'$(eval_gettext 'Show this window at next startup')'</label>
   </checkbox>

   <hbox spacing="10" space-expand="true" space-fill="true">
   <button can-default="true" has-default="true" use-stock="true" is-focus="true">
   <label>gtk-cancel</label>
   <action>exit:Cancel</action>
   </button>
   <button>
   <label>"'$(eval_gettext 'Tutorial')'"</label>
   <input file stock="gtk-info"></input>
   <action signal="button-press-event">'${open_tutoriel_ctpatental}'</action>
   </button>
   <button ok></button>
   </hbox>
 </hbox>

</vbox>
</window>'


MENU_DIALOG="$(gtkdialog --center --program=WINDOW_DIALOG)"

eval ${MENU_DIALOG}
echo "MENU_DIALOG=${MENU_DIALOG}"


if [ ${CHECKBOX_RESTART} == "false" ] ; then
    echo $nom_distribution | tee $fichier_init_config_user
    echo "Fichier servant à ne pas afficher la fenêtre de bienvenue" | tee -a $fichier_init_config_user
fi

if [ ${EXIT} == "OK" ] ; then

    user=$USER

    pkexec /opt/ctparental/install_ctparental_exec.sh "${user}" "${CREATE_CHILD_COUNT}" "${NAME_CHILD_COUNT}" "${PASSWORD_CHILD_COUNT}" "${CREATE_GUEST_COUNT}"
    status=$?

    # Test installation correcte
    if [ ${status} -eq 0 ] ; then
        zenity --info  --no-wrap --text="$(eval_gettext 'Installation was successful.\n\nRemember to check that the parental control is working properly on the child or guest accounts,\nbefore letting your children use the computer.\nFor greater security autologin was disabled on all accounts.')"
    elif [ ${status} -eq 2 ] ; then
        zenity --error --no-wrap --text="$(eval_gettext 'Unable to install.\n\napt/dpkg database locked.\nTry installing CTparental later.')"
    else
        zenity --error --no-wrap --text="$(eval_gettext 'Installation failed.')"
    fi

    if [[ ${LAUNCH_CONFIG_CTPARENTAL} == "true" ]] ; then
        xdg-open https://admin.ct.local
    fi

fi

fi
