#!/bin/bash

# Emmabuntus_fix_swap_use.sh --
#
#   This file permits to automatically use swap in Live mode and to
#   detect and repare the usage of swap after installation
#   for Emmabuntüs distribution.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

. "$HOME/.config/user-dirs.dirs"

clear


###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


repertoire_script=/usr/bin

repertoire_emmabuntus=~/.config/emmabuntus
fichier_init_config_user=${repertoire_emmabuntus}/init_config_fix_swap_use.txt
dir_install_non_free_softwares=/opt/Install_non_free_softwares
fichier_init_config_final=$dir_install_non_free_softwares/.init_config_fix_swap_use.txt

CHECKBOX_RESTART="true"
action=""
nb_swap=0


source /usr/bin/emmabuntus_found_theme.sh
highlight_color=$(FOUND_HIGHLIGHT_COLOR)

swap_actived=$(/sbin/swapon --show |grep -v 'zram\|NAME')

if [ "${swap_actived}" = "" ]; then
    echo "Swap partition not actived"
else
   echo "Swap partition actived"
   exit 1
fi


swap_actived=$(cat /proc/swaps |grep -i /dev |grep -v zram)

if [ "${swap_actived}" = "" ]; then
    echo "Swap partition not actived"
else
   echo "Swap partition actived"
   exit 2
fi


# Activation du swap en mode Live
if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
then

    swap_present=$(sudo /sbin/blkid |grep swap |grep -v zram |cut -d ':' -f1)


    if [ "$swap_present" = "" ]; then
       echo "Swap partition not found"
    else
        echo "Swap partition found"
        swap_on=$(/sbin/swapon --noheadings |grep -v zram)
        if [ "${swap_on}" = "" ]; then
           echo "Swap partition activation"
           sudo /sbin/swapon "$swap_present"
        else
           echo "Swap partition actived"
        fi
    fi

    exit 0

fi


# Correction de l'utilisation du swap en mode installé
if [[ ! ( -f $fichier_init_config_user || -f $fichier_init_config_final ) ]]
then


    swap_on=$(/sbin/swapon --noheadings |grep -v zram)
    swap_present=$(lsblk -fl |grep swap |cut -d ' ' -f1)
    nb_swap=$(echo $swap_present | sed -e 's/\ /\n/g' | wc -l)


    if [ "${nb_swap}" != "1" ]; then
        echo "Number of Swap partition is different of 1 is ${nb_swap} !"
        exit 1

    elif [ "${swap_on}" != "" ]; then
        echo "Swap partition actived"

    elif [ "$swap_present" = "" ]; then
       echo "Swap partition not found on disk"

    else

        swap_on_fstab=$(cat /etc/fstab |grep swap |grep UUID)
        swap_disk=$(/sbin/blkid |grep swap |grep -v zram |cut -d ':' -f1)
        swap_uuid_disk=$(/sbin/blkid |grep swap |grep -v zram |cut -d ' ' -f2 |cut -d '"' -f2)
        swap_uuid_fsfab=$(cat /etc/fstab |grep swap |grep UUID |cut -d ' ' -f1 |cut -d '=' -f2)

        echo "swap_disk=$swap_disk"
        echo "swap_uuid_disk=$swap_uuid_disk"
        echo "swap_uuid_fsfab=$swap_uuid_fsfab"

        if [ "$swap_on_fstab" = "" ]; then
            echo "Swap partition not found on fstab"


            action="NO_SWAP_ON_FSTAB"

        else


            if [ "$swap_uuid_disk" != "" ] && [ "$swap_uuid_fsfab" != "" ] && [ "$swap_uuid_disk" != "$swap_uuid_fsfab" ]; then
                echo "Swap partition found on fstab"

                action="SWAP_INCORRECT_ON_FSTAB"

            elif [ "$swap_uuid_disk" != "" ] && [ "$swap_uuid_fsfab" != "" ] && [ "$swap_uuid_disk" = "$swap_uuid_fsfab" ]; then
                echo "Swap partition found on fstab and off"

                #action="SWAP_OFF"

            fi

        fi
    fi


swap_disk_html="\<span color=\'${highlight_color}\'>$swap_disk\</span>"


# Sélection des messages à afficher en fonction de la langue
# Définition par défaut des messages en Anglais


message_no_swap_on_fstab="\n\
$(eval_gettext 'Your computer has a Swap partition') ($swap_disk_html),\n\
$(eval_gettext 'and it is not used by your system.')\n\
\n\
$(eval_gettext 'Do you want to activate it automatically at system startup?')"

message_swap_incorrect_on_fstab="\n\
$(eval_gettext 'Your computer has a Swap partition') ($swap_disk_html),\n\
$(eval_gettext 'and it is not used by your system.')\n\
\n\
$(eval_gettext 'Do you want to fix this and use this Swap partition automatically at every boot?')"




if [[ ${action} != "" ]]
then

    if [ ${action} == "NO_SWAP_ON_FSTAB" ]; then

        WITH_WINDOWS="600"
        message_fix=${message_no_swap_on_fstab}

    elif [ ${action} == "SWAP_INCORRECT_ON_FSTAB" ]; then

        WITH_WINDOWS="700"
        message_fix=${message_swap_incorrect_on_fstab}

    fi

else

    exit 1

fi


 export WINDOW_DIALOG='<window title="'$(eval_gettext 'Fix Swap usage')'" icon-name="gtk-dialog-question" width_request="'${WITH_WINDOWS}'"  height_request="150" resizable="false">
 <vbox spacing="0">
 <text use-markup="true" wrap="false" xalign="0" justify="3">
 <input>echo "'${message_fix}'" | sed "s%\\\%%g"</input>
 </text>

 <hseparator space-expand="true" space-fill="true"></hseparator>

 <hbox spacing="10" space-expand="false" space-fill="false">
   <checkbox active="'${CHECKBOX_RESTART}'">
   <variable>CHECKBOX_RESTART</variable>
   <label>"'$(eval_gettext 'Show this window at next startup')'"</label>
   </checkbox>

   <hbox spacing="10" space-expand="true" space-fill="true">
   <button cancel></button>
   <button ok></button>
   </hbox>
 </hbox>

 </vbox>
 </window>'



    MENU_DIALOG="$(gtkdialog --center --program=WINDOW_DIALOG)"

    eval ${MENU_DIALOG}
    echo "MENU_DIALOG=${MENU_DIALOG}"

    if [ ${CHECKBOX_RESTART} == "false" ]
    then
        echo $nom_distribution | tee $fichier_init_config_user
        echo "Fichier servant à ne pas lancer le script de corretion de la swap" | tee -a $fichier_init_config_user
    fi

    if [ ${EXIT} == "OK" ]
    then
        if [[ ${nb_swap} == "1" ]] && [[ ${action} != "" ]] && [[ ${swap_disk} != "" ]] && [[ ${swap_uuid_disk} != "" ]] && [[ ${swap_uuid_fsfab} != "" || ${action} == "NO_SWAP_ON_FSTAB" ]]
        then

            user=$USER

            pkexec ${repertoire_script}/emmabuntus_fix_swap_use_exec.sh "$action" "$swap_disk" "$swap_uuid_disk" "$swap_uuid_fsfab" "${user}"

        fi
    fi


fi


