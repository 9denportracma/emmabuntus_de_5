#! /bin/bash

# start_install_free_culture.sh --
#
#   This file permits to install Free Culture.
#
#   Created on 2010-22 by Collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################



clear

repertoire_free_culture=Free_culture
config_free_culture=Config_free_culture


device=`/sbin/blkid -L FREE_CULTURE`

echo "Mount device ${device}"

if ! test -d /${repertoire_free_culture}
then
    sudo mkdir /${repertoire_free_culture}
fi

sudo chmod -R a+rwX /${dir_install_fonts}

sudo umount ${device} /mnt 2> /dev/null

sudo mount ${device} /mnt


if test -f /mnt/${repertoire_free_culture}/install_free_culture.sh
then
    /mnt/${repertoire_free_culture}/install_free_culture.sh

else

    echo "The install Free culture script not found !!!"

fi


