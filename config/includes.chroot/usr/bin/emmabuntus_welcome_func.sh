#!/bin/bash


# emmabuntus_welcome_func.sh --
#
#   This file used to display a Welcome panel for the Emmabuntüs Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


########################################################################################################


. "$HOME/.config/user-dirs.dirs"

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
repertoire_script=/usr/bin
nom_navigateur_defaut=falkon

option=$1
checkbox_restart=$2
welcome_window=$3
#echo "Options : Func"
#echo "option=${option}"
#echo "checkbox_restart=${checkbox_restart}"
#echo "welcome_window=${welcome_window}"

source /usr/bin/emmabuntus_found_theme.sh
highlight_color=$(FOUND_HIGHLIGHT_COLOR)

# Sélection des messages à afficher en fonction de la langue
# Définition par défaut des messages en Anglais

    tutoriel_emmabuntus="/usr/share/Documentation_emmabuntus/Documents/Emmabuntus/Emmabuntus_Info_En.pdf"
    tutoriel_debian_cahier="/usr/share/tbhb/the_beginners_handbook.pdf"
    tutoriel_debian_reference="/usr/share/debian-reference/debian-reference.en.pdf"
    forum_emmabuntus_URL="http://forum.emmabuntus.org"
    video_emmabuntus_URL="http://peertube.emmabuntus.org"
    soutien_emmabuntus_URL="http://soutenir.emmabuntus.org"

if [[ $LANG == fr* ]]
then

    tutoriel_emmabuntus="/usr/share/Documentation_emmabuntus/Documents/Emmabuntus/Emmabuntus_Info.pdf"
    tutoriel_debian_cahier="/usr/share/lcdd/les_cahiers_du_debutant.pdf"
    tutoriel_debian_reference="/usr/share/debian-reference/debian-reference.fr.pdf"

fi

if [ $(which ${nom_navigateur_defaut}) ] ; then
    nom_navigateur=falkon
else
    nom_navigateur=firefox-esr
fi

function FCT_KILL()
{

kill -9 $(ps ax | grep gtkdialog | grep EMMABUNTUS_WELCOME | awk '{print $1}')

}

function FCT_RELOAD()
{

${repertoire_script}/emmabuntus_welcome.sh Init

}

function FCT_WAIT_PROCESS()
{

sleep 2
while ps axg | grep -vw grep | grep -w $1 > /dev/null; do sleep 1; done

}


function FCT_CHECKBOX_RESTART()
{

    if [[ ( ${checkbox_restart} == "false" && ${welcome_window} == "1" ) || $(cat /proc/cmdline | grep -i boot=live) ]]
    then
        sed s/"^Welcome_window=\"[0-9]*\""/"Welcome_window=\"0\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp
        cp ${env_emmabuntus}.tmp ${env_emmabuntus}
        rm ${env_emmabuntus}.tmp
    elif [[ ${checkbox_restart} == "true" && ${welcome_window} == "0" ]]
    then
        sed s/"^Welcome_window=\"[0-9]*\""/"Welcome_window=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp
        cp ${env_emmabuntus}.tmp ${env_emmabuntus}
        rm ${env_emmabuntus}.tmp
    fi

}

if [[ ${option} == EMMABUNTUS ]] ; then
qpdfview --unique ${tutoriel_emmabuntus} &
fi

if [[ ${option} == DEBIAN_CAHIER ]] ; then
qpdfview ${tutoriel_debian_cahier} &
fi

if [[ ${option} == DEBIAN_REFERENCE ]] ; then
qpdfview ${tutoriel_debian_reference} &
fi

if [[ ${option} == OUTILS ]] ; then
if [[ $(ps -A | grep emmabuntus_tool) == "" ]] ; then
${repertoire_script}/emmabuntus_tools.sh &
fi
fi

if [[ ${option} == GEST_PARAMS ]] ; then
if ps -A | grep "xfce4-session" ; then
    if [[ $(ps -A | grep xfce4-settings) == "" ]] ; then
        xfce4-settings-manager &
    fi
elif ps -A | grep "lxqt-session" ; then
    if [[ $(ps -A | grep lxqt-config) == "" ]] ; then
        lxqt-config &
    fi
fi
fi

if [[ ${option} == FORUM ]] ; then
${nom_navigateur} ${forum_emmabuntus_URL} &
fi

if [[ ${option} == VIDEO ]] ; then
${nom_navigateur} ${video_emmabuntus_URL} &
fi

if [[ ${option} == SOUTIEN ]] ; then
${nom_navigateur} ${soutien_emmabuntus_URL} &
fi

if [[ ${option} == LOGITHEQUE ]] ; then
if [[ $(ps -A | grep gnome-software) == "" ]] ; then
gnome-software --mode=overview &
fi
fi

if [[ ${option} == CHECKBOX_RESTART ]] ; then
FCT_CHECKBOX_RESTART
fi

