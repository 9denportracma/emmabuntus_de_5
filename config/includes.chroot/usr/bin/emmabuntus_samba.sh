#!/bin/bash

# Emmabuntus_samba.sh --
#
#   This file informe about limitation
#   of Samba for Emmabuntüs distribution.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

. "$HOME/.config/user-dirs.dirs"

clear

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_titre_window="Samba information"
repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_init_config_user=${repertoire_emmabuntus}/init_samba_info.txt
fichier_init_config_final=$dir_install_non_free_softwares/.init_samba_info.txt
config_samba=/etc/samba/smb.conf

WITH_WINDOWS="700"
WITH_WINDOWS_ERROR="400"
CHECKBOX_RESTART="true"
SHARE_NOM=""
CHECKBOX_WRITE="false"

source /usr/bin/emmabuntus_found_theme.sh
highlight_color=$(FOUND_HIGHLIGHT_COLOR)
#highlight_color=orange

# Affichage de ce menu
if [[ ! ( -f $fichier_init_config_user || -f $fichier_init_config_final ) ]] ; then


dossier_utilisateur="\<span color=\'${highlight_color}\'>\/home\/${USER}\</span>"

creation_dossier="\<span color=\'${highlight_color}\'>sudo mkdir -p \/home\/${USER}_share\</span>"

droit_dossier="\<span color=\'${highlight_color}\'>sudo chmod 777 \/home\/${USER}_share\</span>"

fichier_conf_samba="\<span color=\'${highlight_color}\'>\/etc\/samba\/smb.conf\</span>"

config_samba_exemple="\n\
\<span color=\'${highlight_color}\'>[${USER}_share]\n\
   path = /home/${USER}_share\n\
   writable = yes\n\
   guest ok = yes\n\
   create mode = 0777\n\
   directory mode = 0777\</span>\n\
"

message_info_samba="\n\
$(eval_gettext 'User directories protection in Emmabuntüs prevents')\n\
$(eval_gettext 'using Samba in guest mode for this directory') (${dossier_utilisateur}).\n\
\n\
$(eval_gettext 'If you want to use Samba in guest mode, you must:')\n\
- $(eval_gettext 'Create a folder directly under / home') (${creation_dossier})\n\
- $(eval_gettext 'Give all rights to this folder') (${droit_dossier})\n\
- $(eval_gettext 'Add the rights in the Samba configuration') (${fichier_conf_samba}), $(eval_gettext 'see below:')\n\
${config_samba_exemple}\n\
$(eval_gettext 'Would you like to automatically configure the network share in guest mode below?')"


message_dossier_vide="$(eval_gettext 'The name of the folder is empty!')"

message_partage_existe="$(eval_gettext 'The network share already exists in the Samba configuration!')"



    export WINDOW_DIALOG='<window title="'$(eval_gettext 'Information on Samba usage')'" icon-name="gtk-dialog-question" width_request="'${WITH_WINDOWS}'"  height_request="450" resizable="false">
    <vbox spacing="0">
    <text use-markup="true" wrap="false" xalign="0" justify="3">
    <input>echo "'${message_info_samba}'" | sed "s%\\\%%g"</input>
    </text>

    <hbox>
    <text>
       <label>"'$(eval_gettext 'Sharing in')' /home/"</label>
    </text>
    <entry activates_default="true">
       <default>'${USER}_share'</default>
       <variable>SHARE_NOM</variable>
    </entry>
    </hbox>

    <hseparator space-expand="true" space-fill="false"></hseparator>

    <checkbox active="'${CHECKBOX_WRITE}'">
    <variable>CHECKBOX_WRITE</variable>
    <label>"'$(eval_gettext 'Enable write share')'"</label>
    </checkbox>

    <hseparator space-expand="true" space-fill="true"></hseparator>

      <hbox spacing="10" space-expand="false" space-fill="false">
      <checkbox active="'${CHECKBOX_RESTART}'">
      <variable>CHECKBOX_RESTART</variable>
      <label>"'$(eval_gettext 'Show this window at next launch')'"</label>
      </checkbox>

      <hbox spacing="10" space-expand="true" space-fill="true">
      <button can-default="true" has-default="true" use-stock="true" is-focus="true">
      <label>gtk-cancel</label>
      <action>exit:Cancel</action>
      </button>
      <button ok></button>
      </hbox>
    </hbox>

    </vbox>
    </window>'



    MENU_DIALOG="$(gtkdialog --center --program=WINDOW_DIALOG)"

    eval ${MENU_DIALOG}
    echo "MENU_DIALOG=${MENU_DIALOG}"

    if [ ${CHECKBOX_RESTART} == "false" ]
    then
        echo $nom_distribution | tee $fichier_init_config_user
        echo "Fichier servant à ne pas lancer ce script" | tee -a $fichier_init_config_user
    fi

    if [ ${EXIT} == "OK" ]
    then


        if [[ ${SHARE_NOM} == "" ]]
        then
            echo "${message_dossier_vide}"

            export WINDOW_DIALOG_ERROR='<window title="'${message_erreur}'" icon-name="gtk-info" width_request="'${WITH_WINDOWS_ERROR}'" resizable="false">
            <vbox spacing="0">

            <text use-markup="true" wrap="false" xalign="0" justify="3">
            <input>echo "\n'$message_dossier_vide'" | sed "s%\\\%%g"</input>
            </text>

            <hbox spacing="10" space-expand="false" space-fill="false">
            <button can-default="true" has-default="true" use-stock="true" is-focus="true">
            <label>gtk-ok</label>
            <action>exit:OK</action>
            </button>
            </hbox>

            </vbox>
            </window>'

            MENU_DIALOG_ERROR="$(gtkdialog --center --program=WINDOW_DIALOG_ERROR)"

            exit 1
        fi

        if test -d  "/home/${SHARE_NOM}"
        then

            if [ ${SHARE_NOM} != ${USER}_share ] ; then

                echo "{message_dossier_existe}"

                export WINDOW_DIALOG_ERROR='<window title="'${message_erreur}'" icon-name="gtk-info" width_request="'${WITH_WINDOWS_ERROR}'" resizable="false">
                <vbox spacing="0">

                <text use-markup="true" wrap="false" xalign="0" justify="3">
                <input>echo "\n'$message_dossier_existe'" | sed "s%\\\%%g"</input>
                </text>

                <hbox spacing="10" space-expand="false" space-fill="false">
                <button can-default="true" has-default="true" use-stock="true" is-focus="true">
                <label>gtk-ok</label>
                <action>exit:OK</action>
                </button>
                </hbox>

                </vbox>
                </window>'

                MENU_DIALOG_ERROR="$(gtkdialog --center --program=WINDOW_DIALOG_ERROR)"

            fi

            exit 2
        fi

        if [[ $(cat ${config_samba} | grep -i "/home/${SHARE_NOM}") ]]
        then
            echo "${message_partage_existe}"

             export WINDOW_DIALOG_ERROR='<window title="'${message_erreur}'" icon-name="gtk-info" width_request="'${WITH_WINDOWS_ERROR}'" resizable="false">
            <vbox spacing="0">

            <text use-markup="true" wrap="false" xalign="0" justify="3">
            <input>echo "\n'$message_partage_existe'" | sed "s%\\\%%g"</input>
            </text>

            <hbox spacing="10" space-expand="false" space-fill="false">
            <button can-default="true" has-default="true" use-stock="true" is-focus="true">
            <label>gtk-ok</label>
            <action>exit:OK</action>
            </button>
            </hbox>

            </vbox>
            </window>'

            MENU_DIALOG_ERROR="$(gtkdialog --center --program=WINDOW_DIALOG_ERROR)"

            exit 3
        fi

        pkexec /usr/bin/emmabuntus_samba_exec.sh  "${SHARE_NOM}" ${CHECKBOX_WRITE}


    fi

fi



exit 0

