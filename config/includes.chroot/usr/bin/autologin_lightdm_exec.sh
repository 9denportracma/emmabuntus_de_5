#! /bin/bash

# autologin_lightdm_exec.sh --
#
#   This file permits to add user to autologin for LightDM
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

. "$HOME/.config/user-dirs.dirs"

clear

file_lightdm=/etc/lightdm/lightdm.conf
file_lightdm_tmp=~/lightdm.conf.tmp
file_lightdm_tmp1=~/lightdm.conf.tmp1
utilisateur=${SUDO_USER}

echo "sudo_autologin_lightdm"

# Ajout sécurité suppression des espaces
utilisateur=$(echo "${utilisateur}" | sed "s/ //g")

if [ "${utilisateur}" == "" ]
then

    echo "Exit USER KO"
    exit 1

fi

# Controle présence répertoire utilisateur
if ! test -d "/home/${utilisateur}"
then

    echo "Exit USER not exist"
    exit 2

fi

if [[ $(cat ${file_lightdm} | grep "^autologin-user=") ]]
then

    echo "Exit autologin exist"

else

    sed s/^#autologin-user=$/autologin-user=${utilisateur}/ ${file_lightdm} > ${file_lightdm_tmp1}
    sed s/^#autologin-user-timeout[^$]*/autologin-user-timeout=0/ ${file_lightdm_tmp1} > ${file_lightdm_tmp}


    sudo cp ${file_lightdm_tmp} ${file_lightdm}

    rm ${file_lightdm_tmp1}
    rm ${file_lightdm_tmp}

    echo "Config autologin ${utilisateur} OK"

fi
